# Prueba técnica
## Cargo: Desarrollador backend
## Nivel: Junior

En la siguiente prueba se evualarán tus habilidades técnicas actuales y tu capacidad para resolver problemas, cada punto será evaluado de forma independiente y se tendrá en cuenta no solo el resultado sino también la calidad de la solución.

Para poder evaluar las soluciones **por favor súbelas a un repositorio de Github** con las instrucciones paso a paso para poder ser ejecutadas de manera local.

**Nota**: En el correo en el que recibiste esta prueba técnica se especifica el lenguaje en el cual se debe desarrollar y el tipo de base de datos a utilizar. En caso de alguna duda por favor comunícate al siguiente correo: a.romero@ruedata.com

## Ejercicio 1 - RESTful API

En un ambiente preferiblemente dockerizado desarrolla un API REST que implemente un CRUD de la entidad `Usuario`. Esta entidad simulará a un usuario de una plataforma web, por lo que mínimamente debe tener atributos para almacenar el correo y la contraseña. Adicionalmente, debe crearse la entidad `Rol`, ten en cuenta que un usuario puede tener únicamente un rol pero un rol puede tener muchos usuarios asociados. Los roles posibles deben tener los siguientes nombres: `"administrador"`, `"conductor"`, `"tecnico"`. Al crear, editar u obtener un usuario debe tenerse en cuenta el rol de dicho usuario.

Realiza las validaciones y prácticas que creas pertinentes para el ejercicio.

## Ejercicio 2 - Consumo de API y cron

En un ambiente preferiblemente dockerizado desarrolla un cron que consuma el API https://anapioficeandfire.com/ y almacene en la base de datos cada 30 minutos el nombre y el género de un personaje aleatorio.

## Ejercicio 3 - Algoritmo

Desarrolla un script que encuentre las cadenas de texto que sean palíndromos con más de 3 letras en el siguiente bloque de texto:

`anulalalunapaisajemontanaguaamoraromacomidaluzazulsillagatobotellacamarayosoypalindromocasaverdebanderaventanacangrejolarutanosaportootropasonaturaliniciaracaestoseralodoodolaresdonasbarcosmarcieloaviontierrapaisbicicletaestonoespalindromojugarseverlasalrevesusandounradarenelojorejero`
